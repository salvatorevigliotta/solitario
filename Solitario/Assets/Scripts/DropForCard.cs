﻿using UnityEngine;

[RequireComponent(typeof(CardManager))]
public class DropForCard : Drop
{
    public override bool AcceptDrop(CardManager _card)
    {
        bool accept = false;
        if(!_card.dropFinish)
        {
            DropZone dropZone = GetComponent<DropZone>();
            if (dropZone.enabled)
            {
                CardManager cardManager = GetComponent<CardManager>();
                if (!cardManager.IsBack)
                {
                    DropManager dropManager = GetComponent<DropManager>();
                    if (dropManager.Decks.Count <= 0)
                    {
                        Num = GetComponent<CardManager>().Number;
                        isBlack = GetComponent<CardManager>().isBlack;
                        if (_card.isBlack != isBlack && (_card.Number == (Num - 1)))
                        {
                            _card.EnableDropZone(true);
                            accept = true;
                        }
                    }
                }
            }
        }


        return accept;
    }
}
