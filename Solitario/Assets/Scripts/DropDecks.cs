﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DropManager))]
public class DropDecks : MonoBehaviour
{

    public DropManager dropManager;

    public DecksFront decksFront;

    private bool isInFlip = false;

    // Use this for initialization
    void Start()
    {
        decksFront = FindObjectOfType<DecksFront>();
        dropManager = GetComponent<DropManager>();
       
    }

    public void OnClick()
    {
        if(!isInFlip)
        {
            if (dropManager.Decks.Count > 0)
            {
                DropCard();
            }
            else
            {
                GameManager gm = FindObjectOfType<GameManager>();
                if (gm)
                {
                    gm.AddScore(gm.PenaltyFlipDesk);
                    gm.AddStep();
                }
                ResetCard();
                DropCard();
            }
        }

    }

    private void DropCard()
    {
        isInFlip = true;

        GameManager gm = FindObjectOfType<GameManager>();
        int drawTree = 1;
        if (gm)
        {
            gm.AddScore(gm.PenaltyFilpDeskCard);
            gm.AddStep();
            //If draw tree is true draw tree card else draw one card
            drawTree = gm.isDrawTree ? 3 : 1; 
        }

        StartCoroutine(FlipCard(drawTree));

    }

    private void ResetCard()
    {
        if (decksFront)
            decksFront.ReturnCard();
    }

    IEnumerator FlipCard(int drawTree)
    {
        for (int i = 0; i < drawTree; i++)
        {
            if (dropManager.Decks.Count <= 0)
                break;
            Animator anim = dropManager.Decks[0].gameObject.GetComponent<Animator>();
            if (anim)
            {
                anim.SetTrigger("Flip");
            }

            yield return new WaitForSeconds(dropManager.Decks[0].TimeToFlip /2);

            dropManager.Decks[0].parentToReturnTo = decksFront.gameObject.transform;
            dropManager.Decks[0].gameObject.transform.SetParent(decksFront.gameObject.transform);
            dropManager.Decks[0].gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
            dropManager.Decks[0].droppedDesk = decksFront.GetComponent<DropManager>();
            decksFront.dropManager.Decks.Add(dropManager.Decks[0]);

            
            yield return new WaitForSeconds(dropManager.Decks[0].TimeToFlip/2);
            dropManager.Decks[0].IsBack = false;

            dropManager.Decks.RemoveAt(0);
        }

        isInFlip = false;
    }
}
