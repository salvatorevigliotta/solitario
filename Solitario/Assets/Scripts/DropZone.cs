﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(DropManager))]
[RequireComponent(typeof(Drop))]
public class DropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
    public CardManager Card;
    public int inizialCard = 0;

    public bool acceptCard = true;

    public DropManager dropManager;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("On Pointer Enter");
    }

    public void OnDrop(PointerEventData eventData)
    {
        CardManager _card = eventData.pointerDrag.GetComponent<CardManager>();
        //TODO Regole
        if (_card)
        {
            DropCard(_card);
        }
    }

    internal void DropCard(CardManager _card)
    {
        Drop drop = GetComponent<Drop>();
        if (drop.AcceptDrop(_card))
        {
            _card.parentToReturnTo = transform;
            _card.Position = transform.position;
            Card = _card;
            _card.droppedDesk.RemoveCard(_card);

            //drop Finish zone?
            DropFinish dropFinish = GetComponent<DropFinish>();
            if (!dropFinish && _card.dropFinish)
            {
                _card.dropFinish.DropCard();
                _card.dropFinish = null;
            }

            GameManager gm = GameObject.FindObjectOfType<GameManager>();
            if (gm)
                gm.AddStep();

            dropManager.Decks.Insert(0, _card);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("OnPointerExit");
    }

    // Use this for initialization
    void Start ()
    {
        dropManager = GetComponent<DropManager>();
    }
	
}
