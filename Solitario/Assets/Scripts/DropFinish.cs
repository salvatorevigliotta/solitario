﻿using UnityEngine;

//[RequireComponent(typeof(Image))]
public class DropFinish : Drop
{

    public bool isComplete = false;

    public override bool AcceptDrop(CardManager _card)
    {
        bool accept = false;

        if (_card.Seed == Seed && (_card.Number == Num))
        {
            _card.EnableDropZone(false);
            Num = _card.Number + 1;
            _card.dropFinish = this;

            GameManager gm = FindObjectOfType<GameManager>();
            if(gm)
            {
                gm.AddScore(gm.ScoreDropFinish);
            }

            //Check max card and check game win
            Deck decks = FindObjectOfType<Deck>();
            if (Num == decks.Number.Length)
            {
                isComplete = true;
                gm.CheckWin();
            }
            else
            {
                isComplete = false;
            }

            accept = true;
        }

        return accept;
    }

    public void DropCard()
    {
        Num--;
    }
}
