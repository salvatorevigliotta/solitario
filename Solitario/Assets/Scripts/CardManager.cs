﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(DropZone))]
public class CardManager : MonoBehaviour
{

    public int Number;
    public int Seed;
    public Sprite Picture;

    public Transform parentToReturnTo = null;

    public Transform parentDefault;

    public Vector3 Position;

    public bool isBlack = false;
    public bool IsBack = false;

    [SerializeField]
    private GameObject Back;

    [SerializeField]
    private GameObject Front;

    [SerializeField]
    private GameObject number;

    [SerializeField]
    private GameObject seed;

    [SerializeField]
    private GameObject picture;

    public DropZone dropZone;

    public DropManager droppedDesk;

    public DropFinish dropFinish = null;

    public float TimeToFlip = 1f;

    // Use this for initialization
    void Start ()
    {
        if (number)
        {
            number.GetComponent<Image>().sprite = FindObjectOfType<Deck>().Number[Number];
        }

        if(seed)
        {
            seed.GetComponent<Image>().sprite = FindObjectOfType<Deck>().Seed[Seed];
        }

        if(picture)
        {
            picture.GetComponent<Image>().sprite = Picture;
        }

        if(Seed < 2)
        {
            number.GetComponent<Image>().color = Color.red;
            isBlack = false;
        }
        else
        {
            number.GetComponent<Image>().color = Color.black;
            isBlack = true;
        }

        parentDefault = GameObject.FindGameObjectWithTag("ParentDefault").transform;
        
	}

    // Update is called once per frame
    void Update ()
    {
        if (IsBack)
            SetBack();
        else
            SetFront();

	}

    public void BlockRayCast(bool enable)
    {
        gameObject.GetComponent<CanvasGroup>().blocksRaycasts = enable;
    }

    public void EnableDropZone(bool enable)
    {
        GetComponent<DropZone>().enabled = enable;
    }

    public void SetBack()
    {
        if (Back)
            Back.SetActive(true);

        GetComponent<Draggable>().enabled = false;

        if (Front)
            Front.SetActive(false);
    }

    public void SetFront()
    {
        if (Back)
            Back.SetActive(false);

        GetComponent<Draggable>().enabled = true;

        if (Front)
            Front.SetActive(true);
    }

    public Drop CheckHelp()
    {
        Drop[] drop = FindObjectsOfType<Drop>();

        for(int i = 0; i < drop.Length; i++)
        {
            if (drop[i].AcceptDrop(this))
            {
                return drop[i];
            }            
        }

        return null;
    }
}
