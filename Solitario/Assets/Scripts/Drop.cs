﻿using UnityEngine;

[RequireComponent(typeof(DropManager))]
[RequireComponent(typeof(DropZone))]
public class Drop : MonoBehaviour
{
    public int Seed;
    public int Num;
    public bool isBlack;

    public virtual bool AcceptDrop(CardManager _card)
    {
        return false;
    }
}
