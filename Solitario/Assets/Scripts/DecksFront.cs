﻿using UnityEngine;

public class DecksFront : MonoBehaviour
{
    public DropManager dropManager;
    public DropDecks dropDecks;

    // Use this for initialization
    void Start ()
    {
        dropManager = GetComponent<DropManager>();

        if (!dropDecks)
            dropDecks = FindObjectOfType<DropDecks>();
    }

    public void AddCard(CardManager _card)
    {
        _card.droppedDesk = dropManager;
        dropManager.Decks.Add(_card);
    }

    public void ReturnCard()
    {
        if(dropDecks)
        {
            for(int i = 0; i < dropManager.Decks.Count; i++)
            {
                dropManager.Decks[i].IsBack = true;
                dropManager.Decks[i].parentToReturnTo = dropDecks.gameObject.transform;
                dropManager.Decks[i].gameObject.transform.SetParent(dropDecks.gameObject.transform);
                dropDecks.dropManager.Decks.Add(dropManager.Decks[i]);
            }
            dropManager.Decks.Clear();
        }
    }
}
