﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropManager : MonoBehaviour
{

    public int cardStart = 1;
    public bool isStart = false;
    public List<CardManager> Decks;

    public void RemoveCard(CardManager _card)
    {
        Decks.Remove(_card);
        if(Decks.Count > 0)
        {
            StartCoroutine(FlipFirstCard());
        }
    }

    IEnumerator FlipFirstCard()
    {
        Animator anim = Decks[0].gameObject.GetComponent<Animator>();
        if (anim)
        {
            anim.SetTrigger("Flip");
        }
        yield return new WaitForSeconds(Decks[0].TimeToFlip);
        Decks[0].IsBack = false;
        Decks[0].BlockRayCast(true);
    }
}
