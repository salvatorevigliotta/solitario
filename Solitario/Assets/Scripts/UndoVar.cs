﻿using UnityEngine;

public class UndoVar
{
    public GameObject Card;
    public Vector3 Position;
    public Transform ParentToReturn;
    public bool IsBack;
    public DropZone dropZone;

    public void Undo(GameObject _card, Vector3 _position, Transform _parent, bool _isback, DropZone _dropZone)
    {
        Card = _card;
        Position = _position;
        ParentToReturn = _parent;
        IsBack = _isback;
        dropZone = _dropZone;
    }
}
