﻿
public class DropTable : Drop
{
    public override bool AcceptDrop(CardManager _card)
    {
        bool accept = false;
        DropManager dropManager = GetComponent<DropManager>();

        if (dropManager.Decks.Count <= 0)
        {
            if (_card.Number == Num)
            {
                _card.EnableDropZone(true);
                accept = true;
            }
        }

        return accept;
    }
}
