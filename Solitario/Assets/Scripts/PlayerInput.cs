﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public void Help()
    {
        DropManager[] dropManager = FindObjectsOfType<DropManager>();

        for(int i = 0; i < dropManager.Length; i++)
        {
            if(dropManager[i].Decks.Count > 0)
            {
                if(!dropManager[i].Decks[0].IsBack)
                {
                    Drop helpDrop = dropManager[i].Decks[0].CheckHelp();
                    if (helpDrop)
                    {
                        DropZone helpDropZone = helpDrop.GetComponent<DropZone>();
                        if(helpDropZone)
                        {
                            Debug.Log("Move Card " + dropManager[i].Decks[0] + " to" + helpDrop.name);
                            dropManager[i].Decks[0].gameObject.transform.Translate(helpDrop.gameObject.transform.localPosition);
                            dropManager[i].Decks[0].gameObject.transform.SetParent(helpDrop.gameObject.transform);
                            helpDropZone.DropCard(dropManager[i].Decks[0]);
                        }
                        //Stop
                        return;
                    }
                }

            }
        }

    }
}
