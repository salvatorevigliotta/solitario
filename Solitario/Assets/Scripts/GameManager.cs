﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public int ScoreDropCard = 10;
    public int ScoreDropFinish = 100;
    public int ScoreFlipTableCard = 10;
    public int PenaltyFlipDesk = -100;
    public int PenaltyFilpDeskCard = 0;

    [SerializeField]
    private Text scoreText;
    private int score = 0;

    [SerializeField]
    private Text StepText;
    private int step = 0;

    [SerializeField]
    private GameObject endGameMenu;

    [SerializeField]
    private GameObject pauseMenu;

    public bool isDrawTree = false;
    [SerializeField]
    private Text drawTreeState;

    // Use this for initialization
    void Start ()
    {
        if(endGameMenu)
            endGameMenu.SetActive(false);

        if (pauseMenu)
            pauseMenu.SetActive(false);
	}

    public void AddScore(int _score)
    {
        score += _score;
        scoreText.text = score.ToString();
    }

    public void AddStep()
    {
        step++;
        if (StepText)
            StepText.text = step.ToString();
    }

    public void CheckWin()
    {
        DropFinish[] dropFinish = FindObjectsOfType<DropFinish>();
        if(dropFinish.Length > 0)
        {
            bool isFinish = true;
            for(int i = 0; i < dropFinish.Length; i++)
            {
                if(!dropFinish[i].isComplete)
                {
                    isFinish = false;
                    break;
                }
            }

            if(isFinish)
            {
                if (endGameMenu)
                    endGameMenu.SetActive(true);
            }
        }
    }

    public void EndGame()
    {
        if(endGameMenu)
            endGameMenu.SetActive(true);
    }

    public void DrawTree()
    {
        isDrawTree = !isDrawTree;

        if(drawTreeState)
            drawTreeState.text = isDrawTree ? "Deactive Draw Treee" : "Active Draw tree";
    }

    public void PauseMenu()
    {
        if (pauseMenu)
            pauseMenu.SetActive(!pauseMenu.activeInHierarchy);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
