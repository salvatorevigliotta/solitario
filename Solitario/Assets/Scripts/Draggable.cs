﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CardManager))]
public class Draggable : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private CardManager cardManager;

    public void OnBeginDrag(PointerEventData eventData)
    {
        CardManager _card = GetComponent<CardManager>();
        if (_card)
        {
            if (_card.IsBack)
            {
                _card.IsBack = false;
            }
        }

        DropDecks _decks = transform.parent.gameObject.GetComponent<DropDecks>();
        if(_decks)
        {
            cardManager.parentToReturnTo = _decks.decksFront.gameObject.transform;
            transform.SetParent(cardManager.parentToReturnTo);
            transform.position = cardManager.parentToReturnTo.position;
        }
        else
        {
            cardManager.parentToReturnTo = transform.parent;
            transform.SetParent(cardManager.parentDefault);
        }

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(cardManager.parentToReturnTo);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    // Use this for initialization
    void Start()
    {
        cardManager = GetComponent<CardManager>();
    }

}
