﻿using UnityEngine;
using System.Collections.Generic;

public class Deck : MonoBehaviour
{
    public CardManager CardPrefabs;

    public GameObject StartPosition;

    public Sprite[] Number;

    public Sprite[] Seed;

    public Sprite[] RedPicture;
    public Sprite[] BlackPicture;

    public int DeckCards = 52;

    private DropManager[] dropManager;

    public List<CardManager> DecksCard = new List<CardManager>();

    // Use this for initialization
    void Start ()
    {
        InizializeDeck();
        SelectDropManager();
        StartGame();      
    }

    private void SelectDropManager()
    {
        dropManager = FindObjectsOfType<DropManager>();
    }

    public void StartGame()
    {
        int cardDropped = DecksCard.Count - 1;

        List<DropManager> dropStart = new List<DropManager>();

        for(int d = 0; d < dropManager.Length; d++)
        {
            if (cardDropped > 0)
            {

                //Jump Drop Start
                if (dropManager[d].isStart)
                {
                    dropStart.Add(dropManager[d]);
                }
                else
                {
                    for (int i = 0; i < dropManager[d].cardStart; i++)
                    {
                        if (cardDropped > 0)
                        {

                            if (i < DecksCard.Count)
                            {
                                dropManager[d].Decks.Insert(0, DecksCard[cardDropped]);

                                DecksCard[cardDropped].parentToReturnTo = dropManager[d].transform;
                                DecksCard[cardDropped].gameObject.transform.SetParent(dropManager[d].transform);
                                DecksCard[cardDropped].droppedDesk = dropManager[d];

                                if (i == dropManager[d].cardStart - 1)
                                {
                                    DecksCard[cardDropped].IsBack = false;
                                    DecksCard[cardDropped].BlockRayCast(true);
                                }
                                else
                                {
                                    DecksCard[cardDropped].IsBack = true;
                                }

                                cardDropped--;
                            }
                        }
                    }
                }
            }
        }

        for(int i = 0; i < dropStart.Count; i++)
        {
            int cardToDropp = (cardDropped / dropStart.Count);
            for (int z = 0; z <= cardToDropp; z++)
            {
                if (z <= DecksCard.Count)
                {
                    DecksCard[cardDropped].EnableDropZone(false);
                    dropStart[i].Decks.Insert(0, DecksCard[cardDropped]);
                    DecksCard[cardDropped].IsBack = true;
                    DecksCard[cardDropped].parentToReturnTo = dropStart[i].transform;
                    DecksCard[cardDropped].droppedDesk = dropStart[i];
                    DecksCard[cardDropped].gameObject.transform.SetParent(dropStart[i].transform);
                    cardDropped--;
                }
            }
        }
    }

    private void InizializeDeck()
    {
        int pictureIndex = 0;

        for (int i = 0; i < Seed.Length; i++)
        {
            for (int z = 0; z < Number.Length; z++)
            {
                CardManager _card = Instantiate(CardPrefabs, StartPosition.transform);

                _card.Number = z;
                _card.Seed = i;
                _card.name = "N"+ (z+1) + " S" + (i+1);
                if (z > 9 && i < (Seed.Length - 1) / 2)
                {
                    _card.Picture = RedPicture[pictureIndex];
                    pictureIndex++;
                }
                else if (z > 9 && i >= (Seed.Length - 1) / 2)
                {
                    _card.Picture = BlackPicture[pictureIndex];
                    pictureIndex++;
                }
                else
                {
                    _card.Picture = Seed[i];
                }

                _card.GetComponent<CanvasGroup>().blocksRaycasts = false;
                DecksCard.Add(_card);
            }
            pictureIndex = 0;
        }

        ShuffleDeck();
    }

    private void ShuffleDeck()
    {
        for (int i = 0; i < DecksCard.Count; i++)
        {
            CardManager temp = DecksCard[i];
            int randomIndex = UnityEngine.Random.Range(i, DecksCard.Count);
            DecksCard[i] = DecksCard[randomIndex];
            DecksCard[randomIndex] = temp;
        }
    }
}
